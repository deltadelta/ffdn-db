# -*- coding: utf-8 -*-

SQLALCHEMY_DATABASE_URI = 'sqlite:///../ffdn-db.sqlite'
CRAWLER_MIN_CACHE_TIME = 60 * 60  # 1 hour
CRAWLER_MAX_CACHE_TIME = 60 * 60 * 24 * 14  # 2 week
CRAWLER_DEFAULT_CACHE_TIME = 60 * 60 * 12  # 12 hours
CRAWLER_CRON_INTERVAL = 60 * 20  # used to return valid cache info in the API
SYSTEM_TIME_ZONE = 'Europe/Paris'
LANGUAGES = {
    'en': u'English',
    'fr': u'Français',
}
ISP_FORM_GEOJSON_MAX_SIZE = 256 * 1024
ISP_FORM_GEOJSON_MAX_SIZE_TOTAL = 1024 * 1024
CACHE_NO_NULL_WARNING = True
